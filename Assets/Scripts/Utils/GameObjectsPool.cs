﻿using System.Collections.Generic;
using UnityEngine;

public class GameObjectsPool
{
    private Stack<GameObject> pool;

    private GameObject prefab;
    private Transform parent;
    
    public int Size { get; private set; }

    public GameObjectsPool(int size, GameObject prefab, Transform parent)
    {
        Size = size;
        this.prefab = prefab;
        this.parent = parent;
        pool = new Stack<GameObject>();

        for (int i = 0; i < Size; i++)
        {
            pool.Push(Instantiate());
        }
    }

    public void Return(GameObject pooledObject)
    {
        pooledObject.SetActive(false);
        pool.Push(pooledObject);
    }

    public GameObject Get()
    {
        if (IsEmpty())
        {
            pool.Push(Instantiate());
        }

        GameObject pooledObject = pool.Pop();
        pooledObject.SetActive(true);
        return pooledObject;
    }

    public bool IsEmpty()
    {
        return pool.Count == 0;
    }

    private GameObject Instantiate()
    {
        GameObject created = GameObject.Instantiate(prefab, Vector3.zero, prefab.transform.rotation, parent);
        created.SetActive(false);
        return created;
    }


}
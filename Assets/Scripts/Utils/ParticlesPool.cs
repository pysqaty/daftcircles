﻿using System.Collections.Generic;
using UnityEngine;

public class ParticlesPool
{
    private Stack<ParticleSystem> pool;

    private ParticleSystem prefab;
    private Transform parent;

    public int Size { get; private set; }

    public ParticlesPool(int size, ParticleSystem prefab, Transform parent)
    {
        Size = size;
        this.prefab = prefab;
        this.parent = parent;
        pool = new Stack<ParticleSystem>();

        for (int i = 0; i < Size; i++)
        {
            pool.Push(Instantiate());
        }
    }

    public void Return(ParticleSystem pooledObject)
    {
        pooledObject.gameObject.SetActive(false);
        pool.Push(pooledObject);
    }

    public ParticleSystem Get()
    {
        if (IsEmpty())
        {
            pool.Push(Instantiate());
        }

        ParticleSystem pooledObject = pool.Pop();
        pooledObject.gameObject.SetActive(true);
        return pooledObject;
    }

    public bool IsEmpty()
    {
        return pool.Count == 0;
    }

    private ParticleSystem Instantiate()
    {
        ParticleSystem created = GameObject.Instantiate(prefab, Vector3.zero, prefab.transform.rotation, parent);
        created.gameObject.SetActive(false);
        return created;
    }


}
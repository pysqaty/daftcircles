﻿using System.Collections;
using UnityEngine;

public class Pulse : MonoBehaviour
{

    public float approachSpeed = 0.02f;
    public float growthBound = 2f;
    public float shrinkBound = 0.5f;

    private float currentRatio = 1;
    private Coroutine routine;
    private bool keepGoing = true;
    private Vector3 startSize;

    void OnEnable()
    {
        routine = StartCoroutine(PulseEffect());
    }

    void Awake()
    {
        startSize = transform.localScale;
    }

    private IEnumerator PulseEffect()
    {
        while (keepGoing)
        {
            while (currentRatio != growthBound)
            {
                currentRatio = Mathf.MoveTowards(currentRatio, growthBound, approachSpeed);
                transform.localScale = startSize * currentRatio;
                yield return new WaitForEndOfFrame();
            }

            while (currentRatio != shrinkBound)
            {
                currentRatio = Mathf.MoveTowards(currentRatio, shrinkBound, approachSpeed);
                transform.localScale = startSize * currentRatio;
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
﻿using UnityEngine;

public class StatusBar : MonoBehaviour
{
    public float GetBarSize(float currentValue, float maxValue)
    {
        return currentValue / maxValue;
    }

    public int GetBarPercentage(float currentValue, float maxValue)
    {
        return Mathf.RoundToInt(GetBarSize(currentValue, maxValue) * 100);
    }
}

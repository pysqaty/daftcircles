﻿using UnityEngine;

public interface IObserver
{
    void RemoveBomb(BombManager bomb);
    void RemoveFakeBomb(FakeBombManager bomb);
    void RemoveTapEffect(ParticleSystem tapEffect);
    void EndGame();
}

﻿using UnityEngine;
using TMPro;
using System.Collections;

public class GameManager : MonoBehaviour, IObserver
{
    [Header("Bombs")]
    public GameObject bombPrefab;
    public GameObject fakeBombPrefab;
    public ParticleSystem tapEffect;
    [Space]
    [Header("Environment")]
    public RectTransform area;
    [Space]
    [Header("HUD")]
    public TextMeshProUGUI gameTimeHUD;
    public GameObject gameOverPanel;
    public TextMeshProUGUI finalGameTimeHUD;
    public TextMeshProUGUI highScoreLabelHUD;
    public TextMeshProUGUI continueLabelHUD;
    [Space]
    public SceneLoader sceneLoader;

    //pools
    private GameObjectsPool bombsPool;
    private GameObjectsPool fakeBombsPool;
    private ParticlesPool tapEffectsPool;

    //gameplay
    private RectTransform bombRectTransform;
    private float radius;
    private float levelCorrection = 0;
    private float gameTime = 0f;
    private int bombCounter;
    private int levelBombCounter;
    private float elapsedSpawnTime;
    private bool isGameOver = false;

    //difficulty
    private float TimeToExplosionMultiplier
    {
        get
        {
            return Mathf.Pow(2, (float)levelBombCounter / 4f - 4) / 128f + 1f + levelCorrection;
        }
    }

    private float SecondsBetweenSpawn
    {
        get
        {
            return bombCounter >= 200 ? 0.4f : (-0.003f * (float)bombCounter + 1f);
        }
    }

    void Start()
    {
        bombCounter = 0;
        gameTime = 0;
        elapsedSpawnTime = 0;
        bombsPool = new GameObjectsPool(10, bombPrefab, area);
        fakeBombsPool = new GameObjectsPool(5, fakeBombPrefab, area);
        tapEffectsPool = new ParticlesPool(10, tapEffect, area);
        bombRectTransform = bombPrefab.GetComponent<RectTransform>();
        Pulse pulse = bombPrefab.GetComponent<Pulse>();
        radius = (bombRectTransform.sizeDelta.x * bombRectTransform.localScale.x / 2f) * pulse.growthBound;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit(); //back button
        }

        if (isGameOver)
        {
            StartCoroutine(DelayedAction(2, () =>
            {
                continueLabelHUD.gameObject.SetActive(true);
                if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    sceneLoader.LoadPreviousScene();
                }
            }));
        }
        else
        {
            gameTime += Time.deltaTime;
            gameTimeHUD.text = gameTime.ToString("N2");

            elapsedSpawnTime += Time.deltaTime;

            if (levelBombCounter == 40)
            {
                levelCorrection += 0.07f;
                levelBombCounter = 0;
            }

            if (elapsedSpawnTime > SecondsBetweenSpawn)
            {
                SpawnBomb();
                elapsedSpawnTime = 0;
            }
        }
    }

    #region IObserver
    public void RemoveBomb(BombManager bomb)
    {
        ParticleSystem tap = tapEffectsPool.Get();
        tap.gameObject.transform.position = bomb.transform.position;
        tap.gameObject.GetComponent<Autodestruction>().RegisterObserver(this, tap);
        bombsPool.Return(bomb.gameObject);
        tap.Play();
    }
    public void RemoveFakeBomb(FakeBombManager bomb)
    {
        fakeBombsPool.Return(bomb.gameObject);
    }
    public void RemoveTapEffect(ParticleSystem tapEffect)
    {
        tapEffectsPool.Return(tapEffect);
    }
    public void EndGame()
    { 
        for (int i = 0; i < area.childCount; i++)
        {
            GameObject bomb = area.GetChild(i).gameObject;
            if(bomb.GetComponent<BombManager>() != null)
            {
                bombsPool.Return(bomb);
            }
            else
            {
                fakeBombsPool.Return(bomb);
            }
           
        }
        isGameOver = true;
        finalGameTimeHUD.text = gameTime.ToString("N2");
        gameTimeHUD.gameObject.SetActive(false);
        gameOverPanel.SetActive(true);

        if(gameTime > GameData.HighScore)
        {
            GameData.HighScore = gameTime;
            highScoreLabelHUD.gameObject.SetActive(true);
        }
        else
        {
            highScoreLabelHUD.gameObject.SetActive(false);
        }
    }
    #endregion

    private IEnumerator DelayedAction(float duration, System.Action action)
    {
        yield return new WaitForSeconds(duration);
        action.Invoke();
    }
    private void SpawnBomb()
    {
        float xOffset = bombRectTransform.sizeDelta.x * bombRectTransform.localScale.x / 2f;
        float yOffset = bombRectTransform.sizeDelta.y * bombRectTransform.localScale.x / 2f;
        Vector3 spawnPosition;
        int preventiveCounter = 0;
        do
        {
            float spawnX = Random.Range(-area.sizeDelta.x / 2f + 2 * xOffset, area.sizeDelta.x / 2f - 2 * xOffset); //little border to notch
            float spawnY = Random.Range(-area.sizeDelta.y / 2f + 2 * yOffset, area.sizeDelta.y / 2f - 2 * yOffset);

            spawnPosition = new Vector3(spawnX, spawnY, 0);
            if (preventiveCounter > 100) //probably never happen
            {
                return;
            }
            preventiveCounter++;
        } while (!CheckIfNoOverlapping(spawnPosition));

        float fakeBombParameter = Random.Range(0f, 1f);
        if (fakeBombParameter < 0.1f)
        {
            GameObject bomb = fakeBombsPool.Get();
            bomb.transform.localPosition = spawnPosition;
            FakeBombManager fakeBombManager = bomb.GetComponent<FakeBombManager>();
            fakeBombManager.duration = 3f;
            fakeBombManager.RegisterObserver(this);
        }
        else
        {
            GameObject bomb = bombsPool.Get();
            bomb.transform.localPosition = spawnPosition;
            BombManager bombManager = bomb.GetComponent<BombManager>();
            bombManager.duration = Random.Range(2f, 4f) * (1f / TimeToExplosionMultiplier);
            bombManager.RegisterObserver(this);
        }
        bombCounter++;
        levelBombCounter++;

    }
    private Vector3 GetBottomLeftCorner(RectTransform rt)
    {
        Vector3[] v = new Vector3[4];
        rt.GetWorldCorners(v);
        return v[0];
    }
    private bool CheckIfNoOverlapping(Vector3 position)
    {
        for(int i = 0; i < area.childCount; i++)
        {
            GameObject bomb = area.GetChild(i).gameObject;
            if (bomb.activeInHierarchy)
            {
                Vector3 bombPosition = bomb.transform.localPosition;
                if (((bombPosition.x - position.x) * (bombPosition.x - position.x) +
                    (bombPosition.y - position.y) * (bombPosition.y - position.y)) <=
                    2 * radius * 2 * radius)
                {
                    return false;
                }
            }
        }
        return true;
    }
}

﻿using UnityEngine;

public class Autodestruction : MonoBehaviour
{
    private ParticleSystem ps;
    IObserver observer;

    public void RegisterObserver(IObserver o, ParticleSystem p)
    {
        observer = o;
        ps = p;
    }

    public void Update()
    {
        if (ps)
        {
            if (!ps.IsAlive())
            {
                observer.RemoveTapEffect(ps);
            }
        }
    }
}

﻿using UnityEngine;
using UnityEngine.UI;

public class BombManager : MonoBehaviour
{
    public Image bar;
    public float duration;

    private IObserver observer;
    private StatusBar statusBar;
    private float currentTime;

	void Start()
    {
		statusBar = gameObject.GetComponent<StatusBar>();
	}

    void Update()
    {
        float fillAmount = statusBar.GetBarSize(currentTime, duration);
        bar.fillAmount = fillAmount;

		if(currentTime < duration)
        {
            currentTime += Time.deltaTime;
		}
        else
        {
            observer.EndGame();
        }
	}

    public void RegisterObserver(IObserver o)
    {
        observer = o;
    }

    public void Destroy()
    {
        currentTime = 0;
        observer.RemoveBomb(this);
    }
}

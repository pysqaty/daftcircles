﻿using UnityEngine;

public class FakeBombManager : MonoBehaviour
{
    public float duration;

    private IObserver observer;
    private float currentTime;

    void Update()
    {
        if (currentTime < duration)
        {
            currentTime += Time.deltaTime;
        }
        else
        {
            Destroy();
        }
    }

    public void RegisterObserver(IObserver o)
    {
        observer = o;
    }

    public void Destroy()
    {
        currentTime = 0;
        observer.RemoveFakeBomb(this);
    }

    public void EndGame()
    {
        observer.EndGame();
    }
}

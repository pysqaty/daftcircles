﻿using UnityEngine;

public static class GameData
{ 
    public static float HighScore { get; set; }
    static GameData()
    {
        HighScore = PlayerPrefs.GetFloat("HighScore", 0f);
    }

    public static void SaveHighScore()
    {
        PlayerPrefs.SetFloat("HighScore", HighScore);
        PlayerPrefs.Save();
    }
}

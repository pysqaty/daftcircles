﻿using UnityEngine;

public class HighscoreSaver : MonoBehaviour
{
    void OnEnable()
    {
        DontDestroyOnLoad(this);
    }

    void OnApplicationQuit()
    {
        GameData.SaveHighScore();
    }

    void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            GameData.SaveHighScore();
        }
    }
}

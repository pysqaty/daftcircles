﻿using UnityEngine;
using TMPro;

public class HighscoreLoader : MonoBehaviour
{
    void Start()
    {
        gameObject.GetComponent<TextMeshProUGUI>().text = GameData.HighScore.ToString("N2");
    }

}
